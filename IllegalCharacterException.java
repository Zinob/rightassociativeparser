public class IllegalCharacterException extends RuntimeException {
	private static final long serialVersionUID = -5684025392144073957L;

	public IllegalCharacterException() {
		super();
	}

	public IllegalCharacterException(String message) {
		super(message);
	}
}
