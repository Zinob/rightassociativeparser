/*
 * Assignment 0 for Programming languages and programming paragdigms 
 * at Stockholm University HT 2013.
 * 
 * Authors:
 *         Simon Albinsson
 *         Eric Skoglund
 */

import java.io.StringReader;
import java.io.IOException;

public class EBNFScanner implements Scanner {

	private StringReader input;
	private int length;
	private int current = 0;

	public EBNFScanner(StringReader reader) {
		this.input  = reader;
		if (this.peek() == Scanner.EOF)
			throw new IllegalCharacterException("String cannot be empty");
	}

	public EBNFScanner(String input) {
		this(new StringReader(input));
	}

	@Override
	public char current() {
		if (current != 0)
			return ((char)current) ;
		throw new IndexOutOfBoundsException("No current character");
	}

	@Override
	public char next() {
		try {
			do {
				current = input.read();
			} while (Character.isWhitespace(current));
		} catch (IOException e) {
			System.out.println("Unable to read input");
			System.exit(1);
		}
		
		if (current == -1)
			return Scanner.EOF;
		return (char)current;
	}

	@Override
	public char peek() {
		int temporary = 0;
		try {
			input.mark(1);
			do {
				temporary = input.read();
			} while (Character.isWhitespace(temporary));
			if (temporary == -1)
				return Scanner.EOF;
			input.reset();
		} catch (IOException e) {
			System.out.println("Unable to read input");
			System.exit(1);
		}

		return (char)temporary;
	}
}
