/*
 * Assignment 0 for Programming languages and programming paragdigms 
 * at Stockholm University HT 2013.
 * 
 * Authors:
 *         Simon Albinsson
 *         Eric Skoglund
 */

public class EBNFParser implements Parser {
	private Tokenizer myTokenizer;
	private int parenCounter;

	public EBNFParser(Tokenizer tokenizer) {
		myTokenizer=tokenizer;
	}

	@Override
	public Node parse() {
		AssignNode node = new AssignNode();
		Token t = myTokenizer.next();
		Token n = myTokenizer.next();
		if (n.type()==Token.Type.EQ) {
			node = parseAssignment(t,n);
		} else {
			throw new IllegalCharacterException("The statement must begin with an assigment.");
		}
		return node;
	}
	
	private AssignNode parseAssignment(Token t, Token n) {
		AssignNode currentAssignment = new AssignNode();
		currentAssignment.left=parseIdentifier(t);
		currentAssignment.right=parseExpression(myTokenizer.next());
		if (parenCounter > 0)
			throw new UnmatchedParenthesesException("Unmathed \"(\" found illegal expression");
		return currentAssignment;
	}

	private ExpressionNode parseExpression(Token t) {
		ExpressionNode currentExpression = new ExpressionNode();
		currentExpression.left=parseTerm(t);
		if (myTokenizer.peek().type() == Token.Type.PLUS ||
			myTokenizer.peek().type() == Token.Type.MINUS) {
			currentExpression.operator=myTokenizer.next().value().toString();
			currentExpression.right=parseExpression(myTokenizer.next());
		} else if (myTokenizer.peek().type() == Token.Type.RIGHT_PAREN) {
			if (parenCounter == 0)
				throw new UnmatchedParenthesesException("Unmatched \")\" found illegal expression");
			else
				parenCounter--;
  			myTokenizer.next();
		}
	
		return currentExpression;
	}
	
	private FactorNode parseFactor(Token t) {
		FactorNode currentFactor = new FactorNode();
		if (t.type() == Token.Type.NUMBER){
			currentFactor.node=parseNumber(t);
		} else if (t.type() == Token.Type.LEFT_PAREN) {
			parenCounter++;
			currentFactor.node=parseExpression(myTokenizer.next());
		} else {
			throw new IllegalCharacterException("Right of assignment must be a legal expression");
		}
		return currentFactor;
	}

	private IdentifierNode parseIdentifier(Token t) {
		IdentifierNode currentIdentifier = new IdentifierNode();
		String tokenValue = t.value().toString();
		if (!tokenValue.matches("[a-z]+"))
			throw new IllegalCharacterException("An identifier can only contain the letters a-z");
		else
			currentIdentifier.value = tokenValue;

		return currentIdentifier;
	}

	private NumberNode parseNumber(Token t) {
		NumberNode currentNumber= new NumberNode();
		currentNumber.value=(int)t.value();
		return currentNumber;
	}

	private TermNode parseTerm(Token t) {
		TermNode myTerm = new TermNode();
		myTerm.left=parseFactor(t);
		
		if ( myTokenizer.peek().type() == Token.Type.MULT || myTokenizer.peek().type() == Token.Type.DIV ) {
			myTerm.operator=myTokenizer.next().value().toString();
			myTerm.right=parseTerm(myTokenizer.next());
		}
		return myTerm;
	}
}
