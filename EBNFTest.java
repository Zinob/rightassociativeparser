import java.io.StringReader;

public class EBNFTest {

	public static void main(String[] argin) {
		StringReader reader = new StringReader("a    \n\t =   3  ;");
		EBNFScanner scanner = new EBNFScanner(reader);
		StringBuilder sb = new StringBuilder();
		sb.append(scanner.next());
		System.out.println(scanner.peek());
		sb.append(scanner.next());
		sb.append(scanner.next());
		sb.append(scanner.next());
		System.out.println(sb.toString());
	}
}
