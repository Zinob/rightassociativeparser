/*
 * Assignment 0 for Programming languages and programming paragdigms 
 * at Stockholm University HT 2013.
 * 
 * Authors:
 *         Simon Albinsson
 *         Eric Skoglund
 */

public class EBNFTokenizer implements Tokenizer {
	Scanner myScanner;
	Token currentToken;
	Token TokenCash=null;

	public EBNFTokenizer(Scanner myScanner) {
		this.myScanner=myScanner;	
	}

	@Override
	public Token current() {
		if (this.currentToken == null ) {
			throw new IndexOutOfBoundsException("No current character");
		}
		return this.currentToken;
	}

	@Override
	public Token next() {
		if (TokenCash!=null) {
			Token TempCash=TokenCash;
			TokenCash=null;
			return TempCash;
		}
		char currentChar=myScanner.peek();
		if (Character.isWhitespace(currentChar)){
			consumeWhitespaces();
			currentToken=next();
		} else if (isAlpha(currentChar)){
			currentToken=extractIdentifier();
		} else if (Character.isDigit(currentChar)){
			currentToken=extractNumber();
		} else if (Token.Type.OPERATORS.containsKey(""+currentChar)){
			currentToken=extractOperator();
		} else if (currentChar==Scanner.EOF){
			currentToken=generateEndOfFile();
		} else {
			throw new IllegalCharacterException("Invalid character encountered \""+currentChar+"\" ("+ (int) currentChar +")" );
		}
		return currentToken;
	}

	@Override
	public Token peek() {
		if (TokenCash!=null) {
			return TokenCash;
		} else {
			TokenCash= next();
			return TokenCash;
		}
	}
	
	private Token extractOperator(){
		String operator= Character.toString(myScanner.next());
		
		return new Token(operator,operator,Token.Type.OPERATORS.get(operator));
	}
	
	private Token generateEndOfFile() {
		char character=myScanner.next();
		return new Token(String.valueOf(character),Scanner.EOF,Token.Type.EOF);
	}
	
	private Token extractIdentifier(){
		StringBuilder sb= new StringBuilder();
		while(isAlpha(myScanner.peek())){
			sb.append(myScanner.next());
		}
		String resultingString=sb.toString();
		return new Token(resultingString,resultingString,Token.Type.IDENTIFIER );
	}
	
	private void consumeWhitespaces(){
		while(Character.isWhitespace(myScanner.peek())){
			myScanner.next();
		}
	}

	private boolean isAlpha(char character) {
		return (Character.isAlphabetic(character) &&
				character >= 'a' &&
				character <= 'z')? true : false;
	}
	
	private Token extractNumber(){
		StringBuilder sb= new StringBuilder();
		while(Character.isDigit(myScanner.peek())){
			sb.append(myScanner.next());
		}
		String resultingString=sb.toString();
		return new Token(resultingString,Integer.valueOf(resultingString),Token.Type.NUMBER );
	}
}

