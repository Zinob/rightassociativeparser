/*
 * Assignment 0 for Programming languages and programming paragdigms 
 * at Stockholm University HT 2013.
 * 
 * Authors:
 *         Simon Albinsson
 *         Eric Skoglund
 */

import java.util.HashMap;
import java.util.Map;

public class Visitor {

	public Object visit(Node node) {
		if(node != null) {
			return node.visit(this);
		}
		return null;
	}

	public Object visitAssign(AssignNode n) {
		Map<String, Number> assignments = new HashMap<String, Number>();
		String left = (String) visit(n.left);
		Number right = (Number) visit(n.right);
		assignments.put(left, right);
		return assignments;
	}

	public Object visitExpression(ExpressionNode n) {
		Integer left  = (Integer) visit(n.left);
		Integer right = (Integer) visit(n.right);
		if (n.operator != null){
			if (n.operator.equals("+")) {
				return left+right;
			} else if (n.operator.equals("-")) {
				return left-right;
			} else {
				throw new IllegalArgumentException("operator "+ n.operator +" is not valid");
			}
		} else {
			return left;
		}
	}

	public Object visitTerm(TermNode n) {
		Integer left  = (Integer) visit(n.left);
		Integer right = (Integer) visit(n.right);
		
		if (n.operator != null){
			if (n.operator.equals("*")) {
				System.out.println(left.toString()+" + "+ right.toString());
				return left*right;
			} else if (n.operator.equals("/")) {
				System.out.println(left.toString()+" - "+ right.toString());
				return left/right;
			} else {
				throw new IllegalArgumentException("operator "+ n.operator +" is not valid");
			}
		} else {
			return left;
		}
		
	}

	public Object visitFactor(FactorNode n) {
		return visit(n.node);
	}

	public Object visitIdentifier(IdentifierNode n) {
		return n.value;
	}

	public Object visitNumber(NumberNode n) {
		return n.value;
	}
}
