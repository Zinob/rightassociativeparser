public class UnmatchedParenthesesException extends RuntimeException {
	//private static final long serialVersionUID = -1640565383920357612L;

	public UnmatchedParenthesesException() {
		super();
	}

	public UnmatchedParenthesesException(String message) {
		super(message);
	}
}
